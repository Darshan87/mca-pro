const express = require("express");
const path = require("path");
const hbs = require("hbs");
require("../db/conn");

const app = express();
PORT = 3000;

const Register = require("../modules/registration");
const { request } = require("http");

//Static Folder path (public folder)
const publicPath = path.join(__dirname, "../public");
app.use(express.static(publicPath));

//view folder and partials folder path
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

//set view engine and views(template) folder
app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);

//json
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//routs
app.get("/", (req, res) => {
    res.render("index", { title: "Weather" });
});
app.get("/help", (req, res) => {
    res.render("help", { title: "help" });
});
app.get("/about", (req, res) => {
    res.render("about", { title: "about" });
});
app.get("/Login", (req, res) => {
    res.render("./user/login");
});
app.post("/Login", async (req, res) => {
    // res.send(req.body);
    try {
        const email = req.body.email;
        const password = req.body.password;
        const userEmail = await Register.findOne({ email: email });
        if (userEmail.password === password) {
            res.status(201).render("index");
        } else {
            res.send("Password Not Match");
        }
    } catch (e) {
        res.status(400).send(`invalid email`);
    }
});
app.get("/Registration", (req, res) => {
    res.render("./user/registration");
});
app.post("/Registration", async (req, res) => {
    const p0 = req.body.password;
    const p1 = req.body.confirmpassword;
    try {
        if (p0 === p1) {
            const registerUser = new Register({
                userType: req.body.User,
                name: req.body.name,
                email: req.body.email,
                address: req.body.Address,
                state: req.body.state,
                city: req.body.city,
                zipcode: req.body.zip,
                password: req.body.password,
            });
            const Registered = await registerUser.save();
            res.status(201).render("./user/login");
        } else {
            res.send(`Password not matching`);
        }
    } catch (e) {
        res.status(400).send(`Error ${e}`);
    }
});

app.get("/Profile", (req, res) => {
    res.render("./user/profile");
});

app.listen(PORT, () =>
    console.log(`Server running on http://localhost:${PORT}`)
);
